# Docker Volumes 

Persist data with Docker Volumes 

## Table of Contents
- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

Persist data of a MongoDB container by attaching a Docker volume to it 

## Technologies Used 

* Docker 

* Node.js 

* MongoDB

## Steps 

Step 1: Add data volume to the Docker Compose file 

[Yaml file](/images/01_added_volume.png)

Step 2: Restart Cotainer 

     docker-compose -f docker-compose.yaml down 
     docker-compose -f docker-compose.yaml up 

[Restart 1](/images/04_restar1.png)
[Restart 2](/images/05_restart2.png)

Step 3: Test for data persistence by editing data and checking database 

     localhost:3000
     localhost:8081
[Editing data](/images/01_edited_application.png)
[Checking Mongo Express](/images/02_changes_to_db.png)

Step 4: Restart Container again and check mongo express for data 

     docker-compose -f docker-compose.yaml down 
     docker-compose -f docker-compose.yaml up 

[Restart 1](/images/04_restar1.png)
[Restart 2](/images/05_restart2.png)
[Mongo Express Data](/images/06_persistent_data.png)



## Installation


$ npm install 

$ sudo installer -pkg AWSCLIV2.pkg -target /

## Usage 

Run $ node server.js 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/persisting-data-with-docker-volumes.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using npm test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/persisting-data-with-docker-volumes

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.