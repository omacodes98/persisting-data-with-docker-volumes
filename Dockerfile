From node:13-alpine 

ENV MONGO_DB_USERNAME=admin \
    MONGO_DB_PWD=password

RUN mkdir -p /home/app 

COPY . /Users/oma/Desktop/portfolio/developing-with-docker/app

CMD ["node", "/Users/oma/Desktop/portfolio/developing-with-docker/app/server.js"]